#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'\u041a\u043e\u0441\u043e\u043b\u0430\u043f\u043e\u0432 \u0414\u043c\u0438\u0442\u0440\u0438\u0439'
SITENAME = u'\u041a\u043e\u0441\u043e\u043b\u0430\u043f\u043e\u0432 \u0414\u043c\u0438\u0442\u0440\u0438\u0439: \u043c\u044b\u0441\u043b\u0438 \u0443\u043c\u043d\u044b\u0435 \u0438 \u043d\u0435 \u0442\u043e\u043b\u044c\u043a\u043e.'
SITEURL = u"http://dicos.ru"

PATH = 'content'

TIMEZONE = 'Asia/Novosibirsk'

DEFAULT_LANG = u'ru'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = u'nice-blog'

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#SIDEBAR_DISPLAY = ['about', 'categories', 'tags']

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

GOOGLE_SITE_VERIFICATION = u'yJ35ns77j5pUPUutyBdAd5CwjqK3TGn5prTlyf2LlSo'

PLUGINS = ['plugins.sitemap']

SITEMAP = {
    'exclude': ['tags.html', 'categories.html', 'tag', 'category'],
    'format': 'xml',
}

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'