О книге Макконнелла «Совершенный код»
#####################################
:date: 2010-01-09 22:26
:author: admin
:category: вне категорий
:slug: code_complete
:status: published

Мне давно рекомендовали прочитать эту книгу, дескать, ее необходимо
прочитать каждому программисту. И действительно, фразы наподобие
«Программисты склонны рассматривать менеджеров как низшую ступень
технической эволюции, где-то между одноклеточными организмами и
мамонтами, вымершими в ледниковый период.» дают почувствовать что-то до
боли знакомое, метафоры наподобие

    Бет хотела приготовить тушеное мясо по прославленному рецепту,
    передававшемуся из поколения в поколение в семье ее мужа Абдула.
    Абдул сказал, что его мать солила кусок мяса, перчила, обрезала его
    края, укладывала в горшок, закрывала и ставила в духовку. На вопрос
    Бет «Зачем обрезать оба края?» Абдул ответил: «Не знаю, я всегда так
    делал. Спрошу у мамы». Он позвонил ей и услышал: «Не знаю, просто я
    так всегда делала. Спрошу у твоей бабушки». А бабушка заявила:
    «Понятия не имею, почему вы так делаете. Я делала так потому, что
    мой горшок был маловат».

дают быстро расслабиться от технического текста и в то же время понять
что хотел донести автор.

Во всей книге подробно рассматриваются стадии от проектирования до
тестирования и исправлений ошибок, а также на каждую стадию приведены
свои методологии. Читая книгу можно посмотреть на сколько широка работа
программиста, ведь кроме самого проектирования и написания кода влияют
на разработку выбор инструмента, стиля программирования и написания к
нему комментариев. На реальных примерах показан хороший и плохой код, а
также советы как превратить плохой код и хороший.

P.S. Главе, посвещенной оптимизации кода, есть сравнение эффекта от
методик на разных языках программирования, в списке которых был
`питон <http://python.org/>`__. Все методики оптимизации кода
(превращения из хорошего кода в быстрый) на этом языке не имели эффекта
или приводили к отрицательному результату. Таким образом, Макконнелл
доказал, что быстрый код на питоне это красивый код.

` |так выглядят баги|

.. |так выглядят баги| image:: //img-fotki.yandex.ru/get/60436/44073636.2/0_132e35_dfb1c233_orig
   :class: alignnone size-medium wp-image-16
   :width: 365px
   :height: 172px
   :target: //img-fotki.yandex.ru/get/60436/44073636.2/0_132e35_dfb1c233_orig
