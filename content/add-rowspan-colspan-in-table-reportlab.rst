Добавить rowspan, colspan в таблицу Reportlab
#############################################
:date: 2012-01-03 13:48
:author: admin
:category: python
:slug: add-rowspan-colspan-in-table-reportlab
:status: published

`Reportlab <http://www.reportlab.com/>`__ -- это библиотека для
генерации pdf документов. Мне нужно было создать таблицу, несколько
колонок которой были объединены. Гугл вразумительного ответа не смог
дать, поэтому стал читать официальную документацию. Действительно, на 77
странице вполне себе написано. Попробую дать перевод (вольный):

Класс Table поддерживает объединение столбцов и строк, аналог в html --
rowspan и colspan. Формат объединения такой:

    SPAN, (sc,sr), (ec,er)

где sc, ec объединяемые колонки а sr, er объединяемые строки. Пример:

::

    data = [['Top\nLeft', '', '02', '03', '04'],
           ['', '', '12', '13', '14'],
           ['20', '21', '22', 'Bottom\nRight', ''],
           ['30', '31', '32', '', '']]

::

     t = Table(data, style=[
                       ('GRID',(0,0),(-1,-1),0.5,colors.grey),
                       ('SPAN',(0,0),(1,1)),
                       ('SPAN',(-2,-2),(-1,-1)),
     ])

.. raw:: html

   <br>
   <table class="standard-table">
   <tbody>
   <tr>
   <td rowspan="2" colspan="2">Top Left</td>
   <td>02</td>
   <td>03</td>
   <td>04</td>
   </tr>
   <tr>
   <td>12</td>
   <td>13</td>
   <td>14</td>
   </tr>
   <tr>
   <td>20</td>
   <td>21</td>
   <td>22</td>
   <td rowspan="2" colspan="2">Bottom Right</td>
   </tr>
   <tr>
   <td>30</td>
   <td>31</td>
   <td>32</td>
   </tr>
   </tbody>
   </table>
