AttributeError: 'module' object has no attribute 'Migration'
############################################################
:date: 2013-01-12 14:11
:author: admin
:category: вне категорий
:slug: attributeerror-module-object-has-no-attribute-migration
:status: published

В проекте, над которым сейчас работаю, используется south -- стороннее
 приложение для фреймворка джанго. Суть в следующем: в джанге есть
библиотека для работы с базами данных. Работа с базой данных
осуществляется через специальные классы-модели, которые пишет сам
программист, исходя из структуры базы данных.

Периодически, базу данных необходимо изменять, например добавить пару
полей в таблицу. Обычно, эта работа происходит так: добавляются в
модель-таблицу новые "поля" (возможно, корректнее всего назвать
атрибутами класса). Затем, выполняем в терминале команду вычислить
изменения между тем, что было и тем, что стало. В этот момент создаются
файлы изменений. Потом выполняется команда применить эти изменения в
базе данных.

Вчера, во время применения изменений появилась такая ошибка:

    | Traceback (most recent call last):
    | File "./manage.py", line 15, in <module>
    | execute\_manager(settings)
    | File
      "/somepath/local/lib/python2.7/site-packages/django/core/management/\_\_init\_\_.py",
      line 438, in execute\_manager
    | utility.execute()
    | File
      "/somepath/local/lib/python2.7/site-packages/django/core/management/\_\_init\_\_.py",
      line 379, in execute
    | self.fetch\_command(subcommand).run\_from\_argv(self.argv)
    | File
      "/somepath/local/lib/python2.7/site-packages/django/core/management/base.py",
      line 191, in run\_from\_argv
    | self.execute(\*args, \*\*options.\_\_dict\_\_)
    | File
      "/somepath/local/lib/python2.7/site-packages/django/core/management/base.py",
      line 220, in execute
    | output = self.handle(\*args, \*\*options)
    | File
      "/somepathe/local/lib/python2.7/site-packages/south/management/commands/migrate.py",
      line 107, in handle
    | ignore\_ghosts = ignore\_ghosts,
    | File
      "/somepath/local/lib/python2.7/site-packages/south/migration/\_\_init\_\_.py",
      line 166, in migrate\_app
    | Migrations.calculate\_dependencies()
    | File
      "/somepath/local/lib/python2.7/site-packages/south/migration/base.py",
      line 227, in calculate\_dependencies
    | migration.calculate\_dependencies()
    | File
      "/somepath/local/lib/python2.7/site-packages/south/migration/base.py",
      line 358, in calculate\_dependencies
    | for migration in self.\_get\_dependency\_objects("depends\_on"):
    | File
      "/somepath/local/lib/python2.7/site-packages/south/migration/base.py",
      line 338, in \_get\_dependency\_objects
    | for app, name in getattr(self.migration\_class(), attrname, []):
    | File
      "/somepath/local/lib/python2.7/site-packages/south/migration/base.py",
      line 310, in migration\_class
    | return self.migration().Migration
    | AttributeError: 'module' object has no attribute 'Migration'

Эта ошибка ни как не связана с версией south, делал откат с версии 0.7.6
до 0.7.4. Ошибка появлялась из-за того, что в нескольких приложениях
сайта, файлы миграций были пустые. Пришлось просто удалить пустые файлы
и заново инициализировать миграции. После этого, мое изменение в базе
данных прошло успешно.
