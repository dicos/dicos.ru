Установка egenix-mx-base под virtualenv
#######################################
:date: 2011-06-22 14:28
:author: admin
:category: python
:slug: setup-egenix-mx-base-on-virtualenv
:status: published

Несколько человек, в том числе и я столкнулись с проблемой, когда из-под
виртуальной среды невозможно установить модуль egenix-mx-base.

Установка через pip оканчивалась неудачей:

    | pip install egenix-mx-base
    | Downloading/unpacking egenix-mx-base
    | Downloading egenix-mx-base-3.2.0.tar.gz (4.6Mb): 4.6Mb downloaded
    | Running setup.py egg\_info for package egenix-mx-base
    | Installing collected packages: egenix-mx-base
    | Running setup.py install for egenix-mx-base
    | error: None
    | Complete output from command /home/dmitry/web/openerp/bin/python
      -c "import setuptools;
      \_\_file\_\_='/home/dmitry/web/openerp/build/egenix-mx-base/setup.py';
      execfile('/home/dmitry/web/openerp/build/egenix-mx-base/setup.py')"
      install --single-version-externally-managed --record
      /tmp/pip-NueJAA-record/install-record.txt --install-headers
      /home/dmitry/web/openerp/include/site/python2.7:
    | running install
    | running build
    | running mx\_autoconf
    | error: None
    | ----------------------------------------
    | Command /home/dmitry/web/openerp/bin/python -c "import setuptools;
      \_\_file\_\_='/home/dmitry/web/openerp/build/egenix-mx-base/setup.py';
      execfile('/home/dmitry/web/openerp/build/egenix-mx-base/setup.py')"
      install --single-version-externally-managed --record
      /tmp/pip-NueJAA-record/install-record.txt --install-headers
      /home/dmitry/web/openerp/include/site/python2.7 failed with error
      code 1
    | Storing complete log in /home/dmitry/.pip/pip.log

Установка при помощи  easy\_install давала примерно похожий результат:

    | easy\_install egenix-mx-base
    | Searching for egenix-mx-base
    | Reading http://pypi.python.org/simple/egenix-mx-base/
    | Reading http://www.lemburg.com/python/mxExtensions.html
    | Reading http://www.egenix.com/products/python/mxBase/
    | Reading http://www.egenix.com/
    | Best match: egenix-mx-base 3.2.0
    | Downloading
      http://downloads.egenix.com/python/egenix-mx-base-3.2.0.tar.gz
    | Processing egenix-mx-base-3.2.0.tar.gz
    | Running egenix-mx-base-3.2.0/setup.py -q bdist\_egg --dist-dir
      /tmp/easy\_install-gyJnYN/egenix-mx-base-3.2.0/egg-dist-tmp-ZbsIvy
    | Warning: Can't read registry to find the necessary compiler
      setting
    | Make sure that Python modules \_winreg, win32api or win32con are
      installed.
    | error: Setup script exited with error: None

Попытка сборки из исходников так же оканчивалась неудачей:

    | egenix-mx-base-3.2.0$ python setup.py install
    | running install
    | running build
    | running mx\_autoconf
    | error: None

Решение:
~~~~~~~~

Благо на сайте разработчика сказано о том, что можно установить наш
модуль без C компилятора. Но для этого сперва надо знать в какой
кодировке скомпилирован питон: UTF-16 (UCS-2) или UTF-32 (UCS-4). Узнать
это можно так:

    | $ python #запускаем питон
    | >>> import sys
    | >>> print sys.maxunicode

Если число равно 65535, то кодировка UTF-16 (UCS2), если 4294967295, то
UTF-32 (UCS4).

| Для UTF-16 строка установки будет такая:
| easy\_install -i http://downloads.egenix.com/python/index/ucs2/
  egenix-mx-base

| А для UTF-32 строка установки будет выглядеть так:
| easy\_install -i http://downloads.egenix.com/python/index/ucs4/
  egenix-mx-base

Сайт разработчика: \ http://www.egenix.com/products/python/mxBase/
