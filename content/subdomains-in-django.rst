Субдомены в django
##################
:date: 2011-06-16 16:33
:author: admin
:category: django
:slug: subdomains-in-django
:status: published

На сайте `on-day.ru <http://on-day.ry/>`__, понадобилось реализовать
такую штуку: каждый город должен находиться на своем субдомене.
Полезность этого нововведения пусть оценивает СЕО специалист (привет,
Евгений!), тут опишу техническую сторону вопроса.

Какие требования:

#. должна сохраниться легкость управления сайтом;
#. простота реализации;
#. при дальнейшем развитии сайта не должно быть проблем, связанных с
   архитектурой.

Какие варианты реализаций рассматривал:

#. создать субдомены для каждого города, и для субдомена поставить свой
   движок сайта;
#. средствами веб-сервера преобразовывать запросы с subdomain.on-day.ru
   на on-day.ru/subdomain/
#. джангу надо научить работать с субдоменами

Достоинства/недостатки:

У первого способа основной недостаток в том, что придется вручную
создавать, редактировать и удалять субдомены. То есть научить
контент-менеджера работать с панелью управления DNS сервера, настраивать
виртуальные хосты на веб-сервере. Достоинство вижу только одно: большая
гибкость сайта(-ов).

Во втором способе мне не понравилась жесткая привязка к веб-серверу. То
есть, сейчас использую веб-сервер апач. Если захочу заменить веб-сервер
на nginx, придется разбираться с настройками преобразования URL-ов.

Как понял дорогой мой читатель, я выбрал третий вариант реализации. Мне
нужен готовый модуль с хорошей документацией, поддержкой шаблонных тегов
и очень гибкий в настройке. Все, что перепробовал перечислять не буду, я
остановился на модуле
`django-hosts <http://pypi.python.org/pypi/django-hosts/>`__.

Как установить очень хорошо написано в документации. Сейчас я расскажу о
его особенностях, вернее о том, что мне было не очевидно. Пусть у нас
содержание файла hosts будет таким:

    ::

        from django.conf import settings
        from django_hosts import patterns, host

        host_patterns = patterns('',
            host(r'www', settings.ROOT_URLCONF, name='www'),
            host(r'news', 'news.urls', name='news'),
            host(r'(\w+)', 'user.urls', name='user'),
        )

а файл user/urls.py будет иметь вот такой код:

    ::

        from django.conf.urls.defaults import patterns, url

        urlpatterns = patterns('user.views',
        url(r'^$', 'main_user', name='main_user'),
        url(r'^(?P<slug>[^/]+)/$', 'news', name='news'),
        )

Формат тега host\_url такой: {% host\_url название\_url параметр1\_вьюхи
параметр2\_вьюхи on название\_хоста параметр\_хоста1  %} Для примера,
нам нужно вывести ссылки на последние новостей всех пользователей.
Переменную last\_news содержит в себе информацию о последней новости и
создателе этой новости. Код шаблона будет таким:

    {% load hosts %}

    | {% for news\_item in last\_news %}
    | <a href="{% host\_url news news\_item.slug on user
      news.creator.name %}">{{ news.name }}</a>
    | {% endfor %}

При использовании с
пакетом \ `django-debug-toolbar <http://pypi.python.org/pypi/django-debug-toolbar/>`__
модуль django-hosts вел себя некорректно: не всегда понять какой хост от
него требуют. Исправить этот косяк можно только отключив мидлварь
debug\_toolbar.middleware.DebugToolbarMiddleware. Об этом поведении
`отписался <https://github.com/jezdez/django-hosts/issues/3>`__ на своем
недоанглийском разработчику. Если кто владеет английским гораздо лучше
меня и объяснит автору что я имел ввиду, то буду очень признателен
