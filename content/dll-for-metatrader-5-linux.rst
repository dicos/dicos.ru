dll для MetaTrader 5 в linux
############################
:date: 2014-04-22 17:02
:author: admin
:category: Metatrader
:tags: C, metatrader, wine
:slug: dll-for-metatrader-5-linux
:status: published

Уже как месяц изучаю платформу для торговли ценными бумагами MetaTrader
5. В планах у меня стоит написать до лета торговую систему, которая
будет торговать, торговать, торговать и к Новому Году я стану
миллионером. :-)

Отвлекся. Тут я хотел рассказать о первой проблеме, с которой столкнулся
в процессе разработки. Нужно было подключить к скрипту Metatrader-а свою
dll-ку, написанную на C.На момент битвы над ошибкой использовал Xubuntu
13.10 (32 bit), Wine 1.6.1 и MetaTrader 5 (build 910). Для компиляции
dll файлов использовал кроссплатформенный компилятор mingw и mingw-w64

Как ошибку воспроизвести:

Код самой dll-ки (simple.cpp):

::

    extern "C" {
        int qwe() {
            return 1;
        }
    }

Компилируем командой i686-w64-mingw32-g++ -shared -o out.dll simple.cpp

В MetaEditor, Вставляем код, приведенный ниже. Этот программный код
должен вызывать функцию qwe из  dll файла:

::

    #import "out.dll"
    int qwe();
    #import
    void OnStart()
      {

       Print(qwe());
      }

запускаем  этот код, видим, что Metatrader показывает окно, в котором
нужно подтвердить запуск dll файлов.
|076|

Подтверждаем. После подтверждения видим, в фрейме
"инструменты"->"эксперты" надпись
2014.04.22 16:41:57.190 simple (Si-6.14,H1) Cannot load 'C:\\Program
Files\\ОТКРЫТИЕ-Брокер\\MQL5\\Projects\\out.dll'
|077|

В ходе экспериментов обнаружил, что если установить не mingw-w64-i686 а
более старый mingw, то скомпилированный файл метатрейдер без проблем
подключает:

Компилируем: i586-mingw32msvc-g++ -mrtd -shared -o out.dll simple.cpp

Запускаем в MetaEditor код, нажав на кнопку клавиатуры F5. Metatrader
"спрашивает" можно или нет подключить dll файл:
|075|

Подтверждаем. После подтверждения  в фрейме "инструменты"->"эксперты"
единичку. То есть dll-ка заработала.
|078|

Если внимательно приглядеться, то можно увидеть, что если компилируем
при помощи i686-w64-mingw32-g++, то Metatrader пытается загрузить
какую-то библиотеку libgcc\_s\_sjlj-1.dll, а
компилятор i586-mingw32msvc-g++ как-то обходится без нее.

Решение проблемы нашел в интернете: нужно к компилятору добавить
параметр -static-libgcc. Другими словами, компилировать dll файл нужно
так:

::

   i686-w64-mingw32-g++ -static-libgcc -shared -o out.dll simple.cpp

.. |076| image:: //img-fotki.yandex.ru/get/151498/44073636.2/0_132e49_a71be633_orig
   :width: 300px
   :height: 147px
   :target: //img-fotki.yandex.ru/get/107473/44073636.2/0_132e48_8a0c988d_orig
.. |077| image:: //img-fotki.yandex.ru/get/41468/44073636.2/0_132e4a_e290f75b_orig
   :width: 300px
   :height: 84px
   :target: //img-fotki.yandex.ru/get/41468/44073636.2/0_132e4a_e290f75b_orig
.. |075| image:: //img-fotki.yandex.ru/get/107473/44073636.2/0_132e48_8a0c988d_orig
   :width: 300px
   :height: 148px
   :target: //img-fotki.yandex.ru/get/107473/44073636.2/0_132e48_8a0c988d_orig
.. |078| image:: //img-fotki.yandex.ru/get/60537/44073636.2/0_132e4b_f910b026_orig
   :width: 300px
   :height: 96px
   :target: //img-fotki.yandex.ru/get/60537/44073636.2/0_132e4b_f910b026_orig
