Как отправить значение из списка в combobox
###########################################
:date: 2010-11-12 14:19
:author: admin
:category: javascript
:slug: how-to-make-combobox-send-value-field
:status: published

Когда мы используем Ext JS’а ComboBox, если мы не установили скрытое имя
или его id, то отправляется на сервер введенное значение а не значение
поля. Для исправления нужно установить скрытое имя. За подробностями на`Ext JS
ComboBox’ документацию http://extjs.com/deploy/dev/docs/?class=Ext.form.ComboBox.

::

    var unitField = new Ext.form.ComboBox({
        id:'unitField',
        name: 'unit',
        fieldLabel: 'Unit',
        store:unitStore,
        mode: 'remote',
        displayField: 'name',
        valueField: 'id',
        hiddenName : 'unitId',
        allowBlank: false,
        anchor:'95%',
        triggerAction: 'all'
    });

Взял
`отсюда <http://turgaykivrak.wordpress.com/2008/05/19/extjs-20-tip-how-to-make-combobox-send-value-field/>`__
