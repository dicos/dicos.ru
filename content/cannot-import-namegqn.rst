Ошибка в django-gis
###################
:date: 2010-07-19 15:51
:author: admin
:category: python
:slug: cannot-import-namegqn
:status: published

После перезагрузки рабочего компьютера с windows server 2003 меня
порадовала джанга такой ошибкой:

    | F:\\web\\realtor\\mailTooDatabaseBot>retrieveMail.py
    | C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\backend\\\_\_init\_\_.py:6:
    | UserWarning: The \`django.contrib.gis.db.backend\` module was
      refactored and  to \`django.contrib.gis.db.backends\` in 1.2.  All
      functionality of \`SpatialBackend\` has been moved to the \`ops\`
      attribute of the spatial database backend.  A \`SpatialBackend\`
      alias is provided here for backwards-compatibility, but will be
      removed in 1.3.
    | warn('The \`django.contrib.gis.db.backend\` module was refactored
      and '
    | Traceback (most recent call last):
    | File "F:\\web\\realtor\\mailTooDatabaseBot\\retrieveMail.py", line
      23, in <module>
    | from arenda.models import Area, Street, Apartment, Matherial
    | File "F:\\web\\\*\*\*\*\\models.py", line 7, in <module>
    | from django.contrib.gis.db import models
    | File
      "C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\models\\\_\_init\_\_.py",
      line 5, in <module>
    | from django.contrib.gis.db.models.aggregates import \*
    | File
      "C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\models\\aggregates.py",
      line 2, in <module>
    | from django.contrib.gis.db.models.sql import GeomField
    | File
      "C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\models\\sql\\\_\_init\_\_.py",
      line 2, in <module>
    | from django.contrib.gis.db.models.sql.query import GeoQuery
    | File
      "C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\models\\sql\\query.py",
      line 4, in <module>
    | from django.contrib.gis.db.models.fields import GeometryField
    | File
      "C:\\Python26\\lib\\site-packages\\django\\contrib\\gis\\db\\models\\fields\\\_\_init\_\_.py",
      line 3, in <module>
    | from django.contrib.gis.db.backend import SpatialBackend, gqn
    | ImportError: cannot import name gqn

Из-за чего это происходит: в файле
django\\contrib\\gis\\db\\models\\sql\\query.py происходит импорт из
модуля fields класс GeometryField. Грубо говоря, под модулем можно
понимать файл, находящийся в этом же каталоге, так и пакет --
подкаталог, в котором есть файл \_\_init\_\_.py. Что у нас происходило:
в каталоге django/contrib/gis/db/models у нас есть файл fields.py и
каталог fields. Видимо, приоритет при импорте выше у пакетов, чем у
файлов, поэтому загружался ненужный нам (как оказалось устаревший)
пакет.

Решилось очень просто: так как каталог fields был оставлен только для
совместимости с предыдущими версиями, то его переименовал на
fields\_\_temp, ибо вдруг еще пригодится.

UPD: не у одного у меня такая же возникала проблема, решение проблемы
такое же
http://groups.google.com/group/geodjango/browse_thread/thread/88fc94ed57ab153d
