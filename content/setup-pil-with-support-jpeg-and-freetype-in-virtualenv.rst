Установка PIL с поддержкой jpeg и  freetype в virtualenv
########################################################
:date: 2011-07-08 22:29
:author: admin
:category: сисадминство
:slug: setup-pil-with-support-jpeg-and-freetype-in-virtualenv
:status: published

Столкнулся с такой проблемой: даже если установлены пакеты libjpeg
и libfreetype6-dev в конце компиляции PIL пишет такое безобразие:

    | \*\*\* TKINTER support not available
    | \*\*\* JPEG support not available
    | \*\*\* ZLIB (PNG/ZIP) support not available
    | \*\*\* FREETYPE2 support not available
    | --- LITTLECMS support available

Причина такого поведения -- корявый установщик PIL-а. Исправить
положение можно выполнив такие команды:

    | sudo ln -s /usr/lib/i386-linux-gnu/libfreetype.so /usr/lib/
    | sudo ln -s /usr/lib/i386-linux-gnu/libz.so /usr/lib/
    | sudo ln -s /usr/lib/i386-linux-gnu/libjpeg.so /usr/lib/

После создания симлинков еще раз пробуем установить и скомпилировать:
pip install pil

Если и это не поможет, то придется править установщик:

#. вводим 'pip install -I pil --no-install' скачиваем и распаковываем
   исходники PIL в build каталок текущей виртуальной среды;
#. заходим в каталог builds/pil/ и редактируем в редакторе
   файл setup.py;
#. Ищем строку, в которой написано 'add\_directory(library\_dirs,
   "/usr/lib")' (214 строка);
#. добавляем после этой строки строку 'add\_directory(library\_dirs,
   "/usr/lib/i386-linux-gnu")';
#. закрываем редактор и после этого выполняем команду 'pip install -I
   pil --no-download'
