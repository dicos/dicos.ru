Русские буквы в zip архивах, Ubuntu
###################################
:date: 2011-05-10 19:35
:author: admin
:category: сисадминство
:slug: russian-characters-in-zip-archive
:status: published

Нужно установить доработанную версию архиватора zip.

| Для начала нужно добавить репозиторий sudo apt-add-repository
  ppa:frol/zip-i18n
| Потом сделать обновление системы sudo aptitude update && sudo aptitude
  safe-upgrade
