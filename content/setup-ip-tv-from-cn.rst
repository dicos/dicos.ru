Настройка IP телевидения от "Электронного Города"
#################################################
:date: 2011-01-02 13:21
:author: admin
:category: сисадминство
:slug: setup-ip-tv-from-cn
:status: published

У нас имеется шлюз, раздающий интернет. В качестве шлюза был взят
немного устаревший компьютер, в качестве ОС установлена убунта 10.10.
Чтобы избежать недомолвок сообщаю, о том, что интернет этот компьютер
уже разадавал

Для начала нужно установить
`igmpproxy <http://sourceforge.net/projects/igmpproxy/>`__:

    | качаем:
    | wget
      http://downloads.sourceforge.net/project/igmpproxy/igmpproxy/0.1/igmpproxy-0.1.tar.gz
    | распаковываем:
    | tar -xvf igmpproxy-0.1.tar.gz
    | заходим в каталог:
    | cd igmpproxy-0.1/
    | запускаем скрипт для поиска необходимых библиотек.
    | ./configure
    | у меня на  этом этапе появилась ошибка о том, что нет компилятора C++. Текст ошибки не помню, эту запись пишу по памяти. Устанавливаем:
    | aptitude install gcc
    | еще раз запускаем  ./configure
    | запускаем процесс компиляции:
    | make
    | устанавливаем:
    | make install

После успешного выполнения команд, настраиваем то, что установили:
nano /usr/local/etc/igmpproxy.conf
Содержимое моего файла такое:

    | quickleave
    | phyint eth0 upstream ratelimit 0 threshold 1
    | altnet 10.0.0.0/8
    | altnet 178.0.0.0/8
    | altnet 238.0.0.0/8
    | altnet 239.0.0.0/8
    | altnet 224.0.0.0/8
    | phyint eth1 downstream  ratelimit 0  threshold 1
    | altnet 192.168.1.0/24
    | phyint eth2 disabled

eth0 -- интерфейс, в который "воткнут" интернет
eth1 -- интерфейс, в котором "воткнута" локальная сеть

Пробуем запустить: /usr/local/sbin/igmpproxy
/usr/local/etc/igmpproxy.conf&
Проверяем: sysctl -a \| grep forward \| grep v4
результат п римерно должен быть таким:

    | net.ipv4.conf.all.forwarding = 1
    | net.ipv4.conf.all.mc\_forwarding = 1
    | net.ipv4.conf.default.forwarding = 1
    | net.ipv4.conf.default.mc\_forwarding = 0
    | net.ipv4.conf.lo.forwarding = 1
    | net.ipv4.conf.lo.mc\_forwarding = 0
    | net.ipv4.conf.eth0.forwarding = 1
    | net.ipv4.conf.eth0.mc\_forwarding = 1
    | net.ipv4.conf.eth1.forwarding = 1
    | net.ipv4.conf.eth1.mc\_forwarding = 1
    | net.ipv4.ip\_forward = 1

Далее, настраиваем правила для фаервола (сети не самый мой лучший конек,
возможно что-то сделано неоптимально):

    | iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
    | iptables -A INPUT -i lo -j ACCEPT
    | iptables -A INPUT -p igmp -j ACCEPT
    | iptables -A FORWARD -s 178.49.xxx.xxx/24 -p udp -j ACCEPT
    | iptables -A INPUT -s 192.168.1.0/255.255.255.0 -i eth1 -j ACCEPT
    | iptables -A FORWARD -s 192.168.1.0/24 -p udp -j ACCEPT
    | iptables -t nat -A POSTROUTING -s 192.168.1.0/255.255.255.0 -j MASQUERADE

Проверяем, все должно работать. Если нет, то ищем проблемы, что сделано
не так :-)
Если все заработало, то создаем скрипт запуска нашего igmpproxy:

    | touch /etc/init.d/igmpproxy
    | nano /etc/init.d/igmpproxy

у нас должен открыться редактор, в него вписываем:

    | #!/bin/sh
    | /usr/local/sbin/igmpproxy /usr/local/etc/igmpproxy.conf&

Перезагружаем шлюз, все должно работать.
Если ни чего не получается, то запускаем igmpproxy в режиме
отладки: /usr/local/sbin/igmpproxy /usr/local/etc/igmpproxy.conf -d
Должны появиться такие сообщения:

    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.52 for group 239.1.1.3, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.52 for group 239.1.1.3, is not in any valid net for upstream VIF.
    | The source address 178.49.131.35 for group 239.1.1.2, is not in any valid net for upstream VIF.
    | The source address 178.49.131.52 for group 239.1.1.3, is not in any valid net for upstream VIF.

Тогда в файле /usr/local/etc/igmpproxy.conf дописываем появившийся ip адрес: altnet 239.0.0.0/8 ко всем прочим altnet-ам
