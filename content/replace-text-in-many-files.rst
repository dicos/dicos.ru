Замена текста во многих файлах
##############################
:date: 2011-07-08 22:04
:author: admin
:category: вне категорий
:slug: replace-text-in-many-files
:status: published

Иногда требуется заменить текст на другой текст в нескольких файлах.
Чтобы делать это автоматически использую такой скрипт:

    find . -name \\\*html -print0 \| xargs -0 sed
    's%some\_text%new\_text%g' -i

Он ищет фразу some\_text в файла, названия которых оканчиваются на html
и заменяет фразу some\_text на new\_text.

Эту штуку написал некто Liz с конференции linux@conference.jabber.ru.
Спасибо ему огромное.

 
