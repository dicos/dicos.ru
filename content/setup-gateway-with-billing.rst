Ставим шлюз с биллингом
#######################
:date: 2010-04-05 13:53
:author: admin
:category: сисадминство
:slug: setup-gateway-with-billing
:status: published

В шлюзе (компьютер, который обеспечивает подступ в интернет другим
компьютерам организации) должно стоять 2 сетевых карты: одна для
подключения внутренней сети, вторая для подключения интернета.

Сперва проверим, определились ли сетевые карты, введя команду

    ifconfig -a

    Результат выполнения команды:

    | test@test-gateway:~$ ifconfig -a
    | eth0      Link encap:Ethernet  HWaddr e0:cb:4e:1c:29:ba
    | inet addr:192.168.1.99  Bcast:192.168.1.255  Mask:255.255.255.0
    | inet6 addr: fe80::e2cb:4eff:fe1c:29ba/64 Диапазон:Ссылка
    | UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    | RX packets:3400 errors:0 dropped:0 overruns:0 frame:0
    | TX packets:410 errors:0 dropped:0 overruns:0 carrier:0
    | коллизии:0 txqueuelen:100
    | RX bytes:568554 (568.5 KB)  TX bytes:53278 (53.2 KB)
    | Память:fbee0000-fbf00000

    | eth1      Link encap:Ethernet  HWaddr e0:cb:4e:1c:27:d8
    | BROADCAST MULTICAST  MTU:1500  Metric:1
    | RX packets:0 errors:0 dropped:0 overruns:0 frame:0
    | TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
    | коллизии:0 txqueuelen:1000
    | RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
    | Память:fbde0000-fbe00000

    | lo        Link encap:Локальная петля (Loopback)
    | inet addr:127.0.0.1  Mask:255.0.0.0
    | inet6 addr: ::1/128 Диапазон:Узел
    | UP LOOPBACK RUNNING  MTU:16436  Metric:1
    | RX packets:0 errors:0 dropped:0 overruns:0 frame:0
    | TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
    | коллизии:0 txqueuelen:0
    | RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

    test@test-gateway:~$

У меня сетевая карта eth0 подключена к интернету, eth1 к локальной сети.
При установке системы (ubuntu server 9.10) подключение к интернету было
настроено. Разберемся, как подключится к локальной сети. IP адрес
локальной сети 192.168.3.1 маска: 255.255.255.0 шлюза нет. Для настройки
вводим команды изменения настроек для сетевой карты eth1 и перезагружаем
сеть

    | sudo ifconfig eth1 192.168.3.1 netmask 255.255.255.0
    | sudo /etc/init.d/networking restart

Подсоединяем ко второй сетевой карте другой компьютер, назначаем ему
адрес (я назначил 192.168.3.2) и пробуем этот компьютер попинговать,
должно работать. Более подробно настройка сети описана в статье
«\ `Настройка локальной сети в
Linux <http://itshaman.ru/articles/54/nastroika-lokalnoi-seti-v-linux>`__\ ».

Далее, настроим фаервол так, чтобы из внутренней сети был виден
интернет, а из интернета внутренняя сеть была недоступна. Для начала
создадим пароль для пользователя root, в дальнейшем все команды будем
делать из-под него.

    sudo passwd root

потом переходим в режим управления root -ом

    su root

Редактируем файл sysctl.conf командой nano /etc/sysctl.conf . Здесь
находим строку sctl net.ipv4.ip\_forward = 1 и снимаем с нее
комментирование. Сохраняем файл и выходим из редактора. Выполняем
команду echo 1 > /proc/sys/net/ipv4/ip\_forward.

Устанавливаем следующие пакеты:

-  mc -- консольный файловый менеджер для \*nix систем;
-  squid -- прокси-сервер;
-  build-essential libgnademysql-dev iptables-dev libcurl4-openssl-dev
   libfreeradius-dev libssl-dev libmysql++-dev -- необходимые библиотеки
   для установки traffpro
-  mysql-server mysql-client apache2 libapache2-mod-php5 php5
   php5-common php5-curl php5-dev php5-gd php5-idn php-pear php5-imagick
   php5-imap php5-mcrypt php5-memcache php5-mhash php5-ming php5-mysql
   php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc
   php5-xsl apache2-doc apache2-mpm-prefork apache2-utils libexpat1
   ssl-cert libmysqlclient15-dev -- эти пакеты нужны для того, чтобы
   настроить веб-сервер.  Дело в том, что для управления биллинговой
   системой используется веб-интерфейс.

установка очень простая: вводим команду sudo apt-get install mc squid
build-essential libgnademysql-dev iptables-dev libcurl4-openssl-dev
libfreeradius-dev libssl-dev libmysql++-dev mysql-server mysql-client
apache2 libapache2-mod-php5 php5 php5-common php5-curl php5-dev php5-gd
php5-idn php-pear php5-imagick php5-imap php5-mcrypt php5-memcache
php5-mhash php5-ming php5-mysql php5-pspell php5-recode php5-snmp
php5-sqlite php5-tidy php5-xmlrpc php5-xsl apache2-doc
apache2-mpm-prefork apache2-utils libexpat1 ssl-cert
libmysqlclient15-dev -y . Параметр -y нужен для того, чтобы не система
не спрашивала о подтверждении установки дополнительных пакетов.

В ходе установки потребуется ввести пароль от mysql. Задать можно любой.
главное его запомнить, он еще пригодится.

Удалим прокси-сервер squid из автозагрузки, выполнив команду sudo
update-rc.d -f squid remove . Далее, нам надо остановить сам squid. Для
этого выполняем команду sudo /etc/init.d/squid stop

После установки этих пакетов, нам нужно создать пустую базу данных MySQL
 и добавить пользователя к ней. Для этого, вводим команду

    mysql -u root -p

система запросит ввести пароль. Вводим тот пароль, который мы вводили
при установке MySQL. Если забыли, то `восстанавливаем
забытое <http://www.guruadmin.ru/page/vosstanavlivaem-parol-root-v-mysql>`__.

После того, как «зашли» в базу mysql создаем базу данных, и
пользователя, которому назначаем привелегии

    | mysql>create database traffpro;
    | mysql>GRANT ALL PRIVILEGES ON traffpro.\* TO
      traffpro\_user@'localhost' IDENTIFIED BY 'MY\_PASSWORD' WITH GRANT
      OPTION;

где traffpro -- название базы данных, traffpro\_user -- имя
пользователя, MY\_PASSWORD -- пароль пользователя.

далее заходим на сайт traffpro.ru, идем в раздел скачать, заходим в
каталог release/BASH\_INSTALLER и находим последний релиз traffpro. на
момент 02.04.2010 это будет архив
`traffpro.free.1.3.4-01.tar.gz <http://download.upit-systems.com/release/BASH_INSTALLER/traffpro.free.1.3.4-01.tar.gz>`__
. Качаем командой wget адрес\_архива (например, wget
http://download.upit-systems.com/release/BASH\_INSTALLER/traffpro.free.1.3.4-01.tar.gz)

Распакуем скачаный нами архив в каталог tmp командой tar -xvf
traffpro.free.1.3.4-01.tar.gz -C /tmp

Итак, подготовительные операции мы осуществили, теперь займемся
непосредственной установкой билинга. Для этого заходим в распакованный
каталог с установщиком и запускаем его. Так как переходим в каталог
командой cd  /tmp/traffpro.tree.1.3.4/.  В этом каталоге запускаем сам
установщик командой sudo sh install.sh.

Отвечаем на вопросы так:

Лично у меня графическая установка не заработала, так как не был
установлен пакет dialog, да и Бог с ней - на предложение запустить
установку в графическом режиме (Start TraffPro installation in graphic
mode? [y/n]) отвечаем - n

Выбираем тип дистрибутива - 1 - Debian/Ubuntu or other

Собрать демон контроля (y) или использовать уже собранный (n)? - y

Собрать демон таймера (y) или использовать уже собранный (n)? - y

Сохранить предыдущие настройки TraffPro (в директорию
/var/traffpro\_back)? [y/n] - n

Обновляем систему (y) или устанавливаем (n)? - n

Настроить систему сейчас (y) или оставить настройки по-умолчанию (n)? -
y

Укажите путь установки TraffPro (по-умолчанию - /opt/traffpro) -
оставляем по-умолчанию

Запускать TraffPro в режиме демона? [y/n] - y

Укажите расположение базы данных (по-умолчанию - localhost) - y

Введите имя пользователя базы данных MySQL (по-умолчанию - root) -
traffpro\_user

Введите пароль пользователя базы данных MySQL (по-умолчанию - пустой) -
MY\_PASSWORD

Введите имя базы данных MySQL (по-умолчанию - traffpro) - оставляем
по-умолчанию

Включить контроль MAC адресов? [y/n] - n

Включить детализацию по портам [y/n]? - y

Включить учет трафика и защиту сервера? [y/n] - n

Укажите внешний интерфейс (по-умолчанию - eth0) - пока оставляем eth0

Включить детализацию WWW страниц? [y/n] - y

Введите внешний IP-адрес на интерфейсе для того, чтобы поднять NAT (или
оставьте пустым, для того, чтобы поднять MASQUERADE) - 192.168.1.99 (см
ifconfig в начале)

Укажите IP-адрес для прослушивания монитора (по-умолчанию - 127.0.0.1) -
оставляем по-умолчанию

Укажите порт для прослушивания монитора: (по-умолчанию - 9999) -
оставляем по-умолчанию

Укажите размер очереди пакетов (по-умолчанию - 2048) - оставляем
по-умолчанию

Укажите имя администратора TraffPro (по-умолчанию - admin) - оставляем
по-умолчанию

Укажите пароль администратора TraffPro (по-умолчанию - admin) -
оставляем по-умолчанию

Настроить Traffpro на работу с прозрачным squid? [y/n] - y

Укажите вашу сеть и маску сети (по-умолчанию -
192.168.0.0/255.255.255.0) - 192.168.5.0/255.255.255.0

Укажите порт SQUID для подключения (по-умолчанию - 3128) - оставляем
по-умолчанию

Включить встроенный парсер логов squid? [y/n] - y

Укажите лог-файл SQUID (по-умолчанию - /var/log/squid/access.log) -
оставляем по-умолчанию

Включить планировщик заданий? [y/n] - y

Загрузить модули для работы ftp сервисов? [y/n] - y

Добавить администратора в базу данных [y/n]? - y

Запускать TraffPro при загрузке системы? [y/n] - y

Когда установка закончится активируем модуль веб-сервера apache
mod\_rewrite.so -- этот модуль обеспечивает при отсутствии какого-то
запрашиваемого файла на веб-сервере будет вызван какой-то обрабатывающий
скрипт. Для этого выполним команды:

    cd /etc/apache2/mods-available/

    sudo a2enmod rewrite

    sudo /etc/init.d/apache2 restart

Если все выполнили нормально, то зайдя по адресу
http://192.168.1.99/traffpro/index.php/admin_login/ должны форму входа в
блок администраторов. логин и пароль задавали при установки самого
билинга. По умолчанию логин и пароль admin.

Теперь настроим прокси-вервер squid. Открываем файл squid.conf командой
sudo nano /etc/squid/squid.conf

| Добавляем следующие строки в соответствующие разделы  acl localnet src
  192.168.5.0/24
| http\_access allow localnet
| icp\_access allow localnet
| http\_port 192.168.5.1:3128 transparent

| Открываем файл traffpro\_rule.cfg командой sudo nano
  /etc/traffpro/traffpro\_rule.cfg
| пишем  следющий код

    iptables -A INPUT -m tcp -p tcp --dport 22 -j ACCEPT

    iptables -A OUTPUT -m tcp -p tcp --sport 22 -j ACCEPT

    iptables -A INPUT -i eth0 -p tcp -m tcp --dport 3128 -j QUEUE

    iptables -A OUTPUT -o eth0 -p tcp -m tcp --sport 3128 -j QUEUE

    iptables -t nat -A PREROUTING -p tcp --dport 80 -i eth0 -s
    192.168.5.0/24 -j REDIRECT --to-ports 3128

    modprobe ip\_conntrack\_ftp

    modprobe ip\_nat\_ftp

Далее, перезагружаем компьютер, и у нас должно все работать.

Примерный список статей, по которым я настраивал traffpro:

-  http://traffpro.ru/forum/topic\_845
-  http://free.upit-systems.com/forum/topic\_243#post-6
-  http://www.openkazan.info/TraffPro\_NAT\_Ubuntu\_Linux
