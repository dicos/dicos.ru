Подключаем google maps
######################
:date: 2011-01-02 14:03
:author: admin
:category: javascript
:slug: setup-google-maps
:status: published

Нашел небольшую ошибку в документации:

.. raw:: html

   <div>

написано: <script
src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true\_or\_false&amp;key=ABQIAAAA
WAv1xvwwC6CWojach1uhjRRTECATYhurY4IdD6DIW2V
\_sAdH0BSMmf2Kw38QhOD4zVRxXc4DatAwMg" type="text/javascript"></script>
надо: <script
src="http://maps.google.com/maps/api/js?sensor=true&key=ABQIAAAAWA
v1xvwwC6CWojach1uhjRRTECATYhurY4IdD6DIW2V\_sAdH0BS
Mmf2Kw38QhOD4zVRxXc4DatAwMg" type="text/javascript"></script>

.. raw:: html

   </div>
