Как перенести кнопку закрытия вправо
####################################
:date: 2010-12-06 12:03
:author: admin
:category: вне категорий
:slug: how-to-close-button-right
:status: published

Для этого в консоли нужно выполнить команду

    gconftool-2 --type string --set
    /apps/metacity/general/button\_layout 'menu:minimize,maximize,close'
