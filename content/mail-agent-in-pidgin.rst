mail.agent в pidgin-e
#####################
:date: 2010-07-26 21:11
:author: admin
:category: вне категорий
:slug: mail-agent-in-pidgin
:status: published

Способ как подключить mail.Агент к pidgin-у.
http://z00lus.blogspot.com/2009/09/pidgin-mailru-agent.html. Для
подключения нужно иметь аккаунт на
`jabber.ru <http://www.jabber.ru/>`__.

Теперь я буду знать о том, что ко мне пришло письмо гораздо быстрее.
