Нет DjangoUnicodeDecodeError 
#############################
:date: 2010-05-31 18:15
:author: admin
:category: python, базы данных, сисадминство
:slug: djangounicodedecodeerror
:status: published

Недавно администрировал сервер, нужно было установить работающий проект.
Сразу отмечу, что в качестве СУБД был взят MySQL, так как он уже был
установлен и настроен. База данных успешно была создана, кодировка этой
базы utf8. Но начав помещать тестовые данные столкнулся с пролемой:

    | TemplateSyntaxError at /admin/staff/color/
    | Caught DjangoUnicodeDecodeError while rendering: ('ascii',
      '\\xd0\\x9f\\xd0\\xb5\\xd0\\xbf\\xd0\\xb5\\xd0\\xbb\\xd1\\x8c\\xd0\\xbd\\xd1\\x8b\\xd0\\xb9',
      0, 1, 'ordinal not in range(128)')

    |  Request Method: GET
    | Request URL: http://\*\*\*\*\*\*/admin/staff/color/
    | Django Version: 1.2.1
    | Exception Type: TemplateSyntaxError
    | Exception Value:
    | Caught DjangoUnicodeDecodeError while rendering: ('ascii',
      '\\xd0\\x9f\\xd0\\xb5\\xd0\\xbf\\xd0\\xb5\\xd0\\xbb\\xd1\\x8c\\xd0\\xbd\\xd1\\x8b\\xd0\\xb9',
      0, 1, 'ordinal not in range(128)')
    | Exception Location:
      /usr/local/lib/python2.6/dist-packages/django/utils/encoding.py in
      force\_unicode, line 88
    | Python Executable: /usr/bin/python
    | .............................................................................................
    | Template error
    | In template
      /usr/local/lib/python2.6/dist-packages/django/contrib/admin/templates/admin/change\_list.html,
      error at line 95
    | или In template
      /usr/local/lib/python2.6/dist-packages/django/contrib/admin/templates/admin/index.html,
      error at line 70

Разрешил эту проблему так: удалил все таблицы, затем задал кодировку
базы данных utf8, а сопоставление
\`\`utf8_general_ci (раньше сопоставление имело значение utf8_bin) \`\`\ таким
запросом:

    \`\`ALTER DATABASE \`db_akula\` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci\`\`

    Caught DjangoUnicodeDecodeError while rendering: ('ascii', '\xd0\x9f\xd0\xb5\xd0\xbf\xd0\xb5\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd0\xb9', 0, 1, 'ordinal not in range(128)')

+--------------------------------------+--------------------------------------+
| Request Method:                      | GET                                  |
+--------------------------------------+--------------------------------------+
| Request URL:                         | http://db.dicos.ru/admin/staff/color |
|                                      | /                                    |
+--------------------------------------+--------------------------------------+
| Django Version:                      | 1.2.1                                |
+--------------------------------------+--------------------------------------+
| Exception Type:                      | TemplateSyntaxError                  |
+--------------------------------------+--------------------------------------+
| Exception Value:                     | \:\:                                 |
|                                      |                                      |
|                                      | Caught DjangoUnicodeDecodeError      |
|                                      | while rendering: ('ascii', '\xd0\x9f |
|                                      | \xd0\xb5\xd0\xbf\xd0\xb5\xd0\xbb\xd1 |
|                                      | \x8c\xd0\xbd\xd1\x8b\xd0\xb9', 0, 1, |
|                                      | 'ordinal not in range(128)')         |
+--------------------------------------+--------------------------------------+
| Exception Location:                  | /usr/local/lib/python2.6/dist-packag |
|                                      | es/django/utils/encoding.py          |
|                                      | in force\_unicode, line 88           |
+--------------------------------------+--------------------------------------+
| Python Executable:                   | /usr/bin/python                      |
+--------------------------------------+--------------------------------------+
| Python Version:                      | 2.6.4                                |
+--------------------------------------+--------------------------------------+
| Python Path:                         | ['/usr/lib/python2.6',               |
|                                      | '/usr/lib/python2.6/plat-linux2',    |
|                                      | '/usr/lib/python2.6/lib-tk',         |
|                                      | '/usr/lib/python2.6/lib-old',        |
|                                      | '/usr/lib/python2.6/lib-dynload',    |
|                                      | '/usr/lib/python2.6/dist-packages',  |
|                                      | '/usr/lib/pymodules/python2.6',      |
|                                      | '/usr/lib/pymodules/python2.6/gtk-2. |
|                                      | 0',                                  |
|                                      | '/usr/local/lib/python2.6/dist-packa |
|                                      | ges',                                |
|                                      | '/home/db\_akula/www/']              |
+--------------------------------------+--------------------------------------+
| Server time:                         | Пнд, 31 Май 2010 17:58:02 +0700      |
+--------------------------------------+--------------------------------------+
