Установка django-gis
####################
:date: 2010-07-07 09:12
:author: admin
:category: базы данных
:slug: setup-django-gis
:status: published

Я постараюсь описать установку geo django так как я делал у себя на
машинах с установленными операционными системами линукс (убунту), так и
виндовс (сервер 2003). В качестве СУБД выбрал postgres

Установка на убунту 10.04: Итак, для начала нам надо установить все
необходимые компоненты командой

::

    apt-get install binutils libgdal1-1.6.0  postgresql-8.4-postgis postgresql-server-dev-8.4
    libgeoip1 gdal-bin python-gdal libgeos-c1  libgeos-dev proj binutils bzip2 gcc g++ flex make
    python-ctypes xml2-config libxml2-dev python-libxml2 xml2

Тут я перечислил версии пакетов которые поддерживаются моим домашним
компьютером (ubuntu 10.04). Если стоит более новая система, то лучше
всего устанавливать новые версии пакетов. Версии пакетов определяют
цифры в названиях пакетов. Чтобы узнать список пакетов, достаточно
выполнить команду "aptitude search неполное\_название\_пакета", например
найдем пакеты, содержащие название "libgeos":

::

    aptitude search libgeos
    i ibgeos-3.1.0 - Geometry engine for Geographic Information Systems - C++ Library
    i libgeos-c1 - Geometry engine for Geographic Information Systems - C Library
    p libgeos-dev - Geometry engine for GIS - Development files
    p libgeos-doc - Documentation for the GEOS GIS geometry engine library
    p libgeosruby1.8 - GEOS bindings for Ruby

После того как успешно установили все эти пакеты, приступим к созданию
шаблона пространственных баз данных. Я не стал заморачиваться установкой
вручную, как это описано на странице оф. документации, а взял готовый
скрипт с этой же страницы. К сожалению, скрипт не заработал. Проблема
была в том, что в указанном пути отсутствовали файлы для установки
пространственной базы данных. Воспользовавшись поиском в mc нужно найти
каталог, в котором содержится файл spatial\_ref\_sys.sql. Этот каталог и
нужно было подставить в переменную POSTGIS\_SQL\_PATH. Для начала можно
попробовать запустить sh скрипты, которые представлены в оф.
документации. Если эти скрипты не заработают, представляю свою `версию
установщика базы
даннх <http://dicos.ru/wp-content/uploads/2010/06/create_template_postgis.sh.tar>`__.
Внутри архива 2 скрипта с версиями 1.3 и 1.4 первый предназначен для
постгреса версии 8.3 а второй для версии 8.4.

Эти скрипты нужно выполнять от имени пользователя postgres. Сейчас я
покажу на примере установки на постгрес версии 8.3:

::

    root@server: su postgres
    postgres@server: sh create\_template\_postgis-1.3.sh

Если все выполнено верно, то последние слова будут такие:

::

    COMMIT
    VACUUM
    GRANT
    GRANT

Но если заканчивается словами «ERROR: relation "spatial\_ref\_sys" does
not exist» то, перед тем, как запустить другой скрипт нужно будет
удалить уже созданную базу данных. Я сделал это так: запустил
pgAdminIII, залез в базу данных postgres, потом в "каталоги", там выбрал
каталог PostgreSQL, далее перешел в "таблицы", нажал правой кнопкой мыши
на таблицу pg\_database, в раскрывшемся списке выбрал пункт показать
"все строки". Значение ячейки, находящуюся в столбце datistemplate и
строке template\_postgis изменил на False.

|image0|

|image1|

Установить также просто на убунту 8.04 у меня не получилось. Дело в том,
что там лежат уже старые версии библиотек, а django-gis требует более
новые. Поэтому нужно все собирать вручную. Все не так страшно как
кажется: в документации написаны
`команды <http://docs.djangoproject.com/en/1.2/ref/contrib/gis/install/#geos>`__,
которые просто нужно последовательно вводить в консоль.

Установка под виндовс такая: устанавливаем постгрес 8.4, далее качаем
psycopg2 -- это связующее звено между постгресом и питоном. Затем
устанавливаем `GeoDjango
Installer <http://geodjango.org/windows/GeoDjango_Installer.exe>`__.
После того, как установили, проверяем переменные среды: для этого
щелкаем правой кнопкой на «мой компьютер», далее переходим на вкладку
«дополнительно». Нажимаем на кнопку «переменные среды», и правим пути в
которых есть слово postgres. В этих путях нужно заменить 8.3 на 8.4
Иначе выпрыгнет ошибка «django.core.exceptions.ImproperlyConfigured:
Could not import user-defined GEOMETRY\_BACKEND "geos"». То есть нам
надо заменить:

C:\\Program Files\\PostgreSQL\\8.3\\bin; на C:\\Program
Files\\PostgreSQL\\8.4\\bin

То есть:
|image2|

После всех этих махинаций у нас все должно работать. Если что не
понятно, пишите на почту.

документация по установке geodjango (англ.) -- http://docs.djangoproject.com/en/1.2/ref/contrib/gis/install/
оф. сайт psycopg2 -- http://www.stickpeople.com/projects/python/win-psycopg/

.. raw:: html

   <div id="_mcePaste"
   style="position: absolute; left: -10000px; top: 627px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden; text-align: left;">

`ERROR: relation "spatial\_ref\_sys" does not
***exist*** <http://www.google.com/search?hl=ru&pwst=1&&sa=X&ei=t88gTIucFIq6ONPN0Gs&ved=0CBUQvwUoAQ&q=ERROR%3A+relation+%22spatial_ref_sys%22+does+not+exist&spell=1>`__.

.. raw:: html

   </div>

.. |image0| image:: //img-fotki.yandex.ru/get/109111/44073636.2/0_132f26_36c6aa40_L.png
   :width: 456px
   :height: 500px
   :target: //img-fotki.yandex.ru/get/109111/44073636.2/0_132f26_36c6aa40_orig
.. |image1| image:: //img-fotki.yandex.ru/get/62989/44073636.2/0_132e3a_60635f3c_L.png
   :width: 500px
   :height: 157px
   :target: //img-fotki.yandex.ru/get/62989/44073636.2/0_132e3a_60635f3c_orig
.. |image2| image:: //img-fotki.yandex.ru/get/128901/44073636.2/0_132e3b_be06866e_L.gif
   :width: 500px
   :height: 478px
   :target: //img-fotki.yandex.ru/get/128901/44073636.2/0_132e3b_be06866e_orig
