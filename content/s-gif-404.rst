"GET /resources/images/default/s.gif HTTP/1.1" 404
##################################################
:date: 2010-11-01 14:59
:author: admin
:category: javascript
:slug: s-gif-404
:status: published

| Сегодня столкнулся в ExtJs с такой проблемой: одна из картинок для
  создания элемента формы combobox не могла загрузиться:
| "GET /resources/images/default/s.gif HTTP/1.1" 404

| Для того, чтобы исправить это мы пишем в скрипте:
  Ext.BLANK\_IMAGE\_URL = 'путь\_до\_файла/s.gif';
| К примеру, у меня полный путь такой: Ext.BLANK\_IMAGE\_URL =
  '/media/extjs/resources/images/default/s.gif';
