Как сделать свою википедию
##########################
:date: 2011-03-13 12:39
:author: admin
:category: php
:slug: ones-own-wikipedia
:status: published

Моим недавним заказом было создание сайта на основе движка
`mediawiki <http://www.mediawiki.org/>`__. Какие задачи стояли:

-  вставлять description, keywords в страницу;
-  помещать ролики youtube на страницу;
-  изменять title страниц;
-  создать несколько шаблонов страниц;
-  размещать карту яндекса в любом месте.

Программировать, кроме последнего ни чего не надо было, обошелся
готовыми модулями:

Для keywords есть готовое расширение
`MetaKeywordsTag <http://www.mediawiki.org/wiki/Extension:MetaKeywordsTag>`__. С
тегом description все оказалось сложнее из-за того, что многие
расширения либо не работали, либо работали не так как надо (этот тег
оказывался внутри тега body а не head). Решение оказалось самое простое:
открыть MetaKeywordsTag.php, заменить внутри него слова keywords на
description. Вот готовый
`результат <http://dicos.ru/wp-content/uploads/2011/03/MetaDescriptionTag.php_.tar>`__.

Чтобы вставлять видео с ютуба, нужно установить
модуль \ `EmbedVideo <http://www.mediawiki.org/wiki/Extension:EmbedVideo>`__

Расширения, нужное для вставки title, в текущей стабильной версии нет
(1.16), для следующей версии есть. Рекомендовать ни чего не буду, так
как с выходом релиза все может сломаться.

Чтобы сделать более-менее интеллектуальные шаблоны (например карточки)
нужно установить
расширение \ `ParserFunctions <http://www.mediawiki.org/wiki/ParserFunctions>`__.
Пример карточки можно увидеть на странице с описанием
`АК-47 <http://ru.wikipedia.org/wiki/%D0%90%D0%9A-47>`__, она находится
справа.

Подробнее хотел бы остановиться на яндекс картах. Для начала `скачайте
файл <http://dicos.ru/wp-content/uploads/2011/03/YandexMap.php_.tar>`__
и распакуйте его в каталог extensions. Потом откройте файл
LocalSettings.php и добавьте такие строки:

    | $wgYandexMapsKey = 'API-ключ';
    | require\_once( '$IP/extensions/YandexMap.php' );

Если кто не знает, ключ можно получить только зарегистрировавшись на
яндексе на этой странице \ http://api.yandex.ru/maps/keyslist.xml
