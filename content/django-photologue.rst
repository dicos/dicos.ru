django-photologue: установка и настройка
########################################
:date: 2010-03-08 23:30
:author: admin
:category: python
:slug: django-photologue
:status: published

Наткнулся на готовую фото
галерею «\ `django-photologue <http://code.google.com/p/django-photologue/>`__\ »
для джанги. Мне она приглянулась по нескольким причинам:

-  полностью готовое приложение;
-  есть возможность накладывать водные знаки на фотографии;
-  добавлять эффекты (а-ля мокрый пол), разворачивать изображения;
-  готовая админка и все сопутствующие блага цивилизации.

Ко всему прочему, хорошая
`документация <http://code.google.com/p/django-photologue/w/list>`__,
скачать можно как с svn-а так и в виде
`архива <http://code.google.com/p/django-photologue/downloads/list>`__.

Установка не совсем простая, хотя этот процесс описан в документации.
Кратко опишу свои действия:

#. через терминал устанавливаем само приложение;
#. копируем каталог с шаблонами этого приложения в каталог шаблонов
   своего приложения;
#. в файле settings.py указываем путь до файла sample.jpg. Этот файл
   также находится в дистрибутиве.

Недостатком можно отметить отсутствие поддержки названий водных знаков
на русском языке, о которой я уже
`сообщил <http://code.google.com/p/django-photologue/issues/detail?id=152&sort=status>`__.

Еще с одной трудностью столкнулся, что у меня вместо пред просмотра
фотографий была надпись «An "admin\_thumbnail" photo size has not been
defined.». Эта проблема решилась запуском «python manage.py plinit» и
вводом параметров изображений:

    dmitry@dmitry-desktop:~/web/nskgarant$ python manage.py plinit

    | Photologue requires a specific photo size to display thumbnail
      previews in the Django admin application.
    | Would you like to generate this size now? (yes, no):yes

    We will now define the "admin\_thumbnail" photo size:

    | Width (in pixels):80
    | Height (in pixels):60
    | Crop to fit? (yes, no):yes
    | Pre-cache? (yes, no):yes
    | Increment count? (yes, no):no

    A "admin\_thumbnail" photo size has been created.

    Would you like to apply a sample enhancement effect to your admin
    thumbnails? (yes, no):yes

    | Photologue comes with a set of templates for setting up a complete
      photo gallery. These templates require you to define both a
      "thumbnail" and "display" size.
    | Would you like to define them now? (yes, no):yes

    We will now define the "thumbnail" photo size:

    | Width (in pixels):80
    | Height (in pixels):60
    | Crop to fit? (yes, no):yes
    | Pre-cache? (yes, no):yes
    | Increment count? (yes, no):no

    A "thumbnail" photo size has been created.

    We will now define the "display" photo size:

    | Width (in pixels):800
    | Height (in pixels):600
    | Crop to fit? (yes, no):yes
    | Pre-cache? (yes, no):yes
    | Increment count? (yes, no):no

    A "display" photo size has been created.

    Would you like to apply a sample reflection effect to your display
    images? (yes, no):yes

Сперва спрашиваются нужно ли показывать в админке уменьшенные
изображения. В случае положительного ответа запрашивается ширина,
высота, нужно ли обрезать изображения, кешировать ли уменьшенные
изображения и ставить ли на картинках номера изображений. Следующий
вопрос о том, стоит ли применить внесенные изменения к текущим
изображениям.

Далее задаются вопросы о том, стоит ли задать значения для уменьшенного
«большого» изображений. Смысл вводимых параметров совпадает с
перечисленными выше.