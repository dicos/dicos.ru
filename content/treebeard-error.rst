treebeard выдало сообщение об ошибке
####################################
:date: 2010-04-25 22:58
:author: admin
:category: python
:slug: treebeard-error
:status: published

После того, как я выложил на рабочий сервер свой проект, часть сайта,
доступная простым смертным работала нормально с тестовыми данными. Не
работала только закрытая часть, то есть админка. Вернее модуль,
позволяющий создавать страницы с текстом сообщал мне об ошибке.

    | TemplateSyntaxError at /admin/pages/pages/
    | Caught an exception while rendering: Failed lookup for key
      [request] in u'[{\\'action\_index\\': 0, \\'block\\': <Block Node:
      result\_list. Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n \\'>, <django.template.SimpleNode object at
      0x1327a890>, <Text Node: \\'\\n \\'>, <If node>, <Text Node:
      \\'\\n\\'>]>}, {\\'block\\': <Block Node: content. Contents:
      [<Text Node: \\'\\n <div id="content-main"\\'>, <Block Node:
      object-tools. Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n \\'>]>, <Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n <div class="module\\'>, <If node>, <Text Node: \\'"
      id="changelist">\\n \\'>, <Block Node: search. Contents:
      [<django.template.InclusionNode object at 0x13243310>]>, <Text
      Node: \\'\\n \\'>, <Block Node: date\_hierarchy. Contents:
      [<django.template.InclusionNode object at 0x132430d0>]>, <Text
      Node: \\'\\n\\n \\'>, <Block Node: filters. Contents: [<Text Node:
      \\'\\n \\'>, <If node>, <Text Node: \\'\\n \\'>]>, <Text Node:
      \\'\\n \\n <form actio\\'>, <If node>, <Text Node: \\'>\\n \\'>,
      <If node>, <Text Node: \\'\\n\\n \\'>, <Block Node: result\_list.
      Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text Node: \\'\\n
      \\'>, <django.template.SimpleNode object at 0x1327a890>, <Text
      Node: \\'\\n \\'>, <If node>, <Text Node: \\'\\n\\'>]>, <Text
      Node: \\'\\n \\'>, <Block Node: pagination. Contents:
      [<django.template.InclusionNode object at 0x13256a90>]>, <Text
      Node: \\'\\n </form>\\n </div>\\'>]>}, {\\'actions\_on\_bottom\\':
      False, \\'cl\\': <django.contrib.admin.views.main.ChangeList
      object at 0x1324ec90>, \\'has\_add\_permission\\': True,
      \\'media\\': <django.forms.widgets.Media object at 0x1327a450>,
      \\'root\_path\\': None, \\'action\_form\\':
      <django.contrib.admin.helpers.ActionForm object at 0x1327a6d0>,
      \\'actions\_on\_top\\': True, \\'app\_label\\': \\'pages\\',
      \\'is\_popup\\': False, \\'title\\':
      u\\'\\\\u0412\\\\u044b\\\\u0431\\\\u0435\\\\u0440\\\\u0438\\\\u0442\\\\u0435
      pages \\\\u0434\\\\u043b\\\\u044f
      \\\\u0438\\\\u0437\\\\u043c\\\\u0435\\\\u043d\\\\u0435\\\\u043d\\\\u0438\\\\u044f\\'},
      {\\'this\_url\\': u\\'/admin/pages/pages/\\'}, {\\'MEDIA\_URL\\':
      \\'/media/\\'}, {\\'LANGUAGES\\': ((\\'ar\\', \\'Arabic\\'),
      (\\'bn\\', \\'Bengali\\'), (\\'bg\\', \\'Bulgarian\\'), (\\'ca\\',
      \\'Catalan\\'), (\\'cs\\', \\'Czech\\'), (\\'cy\\', \\'Welsh\\'),
      (\\'da\\', \\'Danish\\'), (\\'de\\', \\'German\\'), (\\'el\\',
      \\'Greek\\'), (\\'en\\', \\'English\\'), (\\'es\\',
      \\'Spanish\\'), (\\'et\\', \\'Estonian\\'), (\\'es-ar\\',
      \\'Argentinean Spanish\\'), (\\'eu\\', \\'Basque\\'), (\\'fa\\',
      \\'Persian\\'), (\\'fi\\', \\'Finnish\\'), (\\'fr\\',
      \\'French\\'), (\\'ga\\', \\'Irish\\'), (\\'gl\\',
      \\'Galician\\'), (\\'hu\\', \\'Hungarian\\'), (\\'he\\',
      \\'Hebrew\\'), (\\'hi\\', \\'Hindi\\'), (\\'hr\\',
      \\'Croatian\\'), (\\'is\\', \\'Icelandic\\'), (\\'it\\',
      \\'Italian\\'), (\\'ja\\', \\'Japanese\\'), (\\'ka\\',
      \\'Georgian\\'), (\\'ko\\', \\'Korean\\'), (\\'km\\',
      \\'Khmer\\'), (\\'kn\\', \\'Kannada\\'), (\\'lv\\',
      \\'Latvian\\'), (\\'lt\\', \\'Lithuanian\\'), (\\'mk\\',
      \\'Macedonian\\'), (\\'nl\\', \\'Dutch\\'), (\\'no\\',
      \\'Norwegian\\'), (\\'pl\\', \\'Polish\\'), (\\'pt\\',
      \\'Portuguese\\'), (\\'pt-br\\', \\'Brazilian Portuguese\\'),
      (\\'ro\\', \\'Romanian\\'), (\\'ru\\', \\'Russian\\'), (\\'sk\\',
      \\'Slovak\\'), (\\'sl\\', \\'Slovenian\\'), (\\'sr\\',
      \\'Serbian\\'), (\\'sv\\', \\'Swedish\\'), (\\'ta\\',
      \\'Tamil\\'), (\\'te\\', \\'Telugu\\'), (\\'th\\', \\'Thai\\'),
      (\\'tr\\', \\'Turkish\\'), (\\'uk\\', \\'Ukrainian\\'),
      (\\'zh-cn\\', \\'Simplified Chinese\\'), (\\'zh-tw\\',
      \\'Traditional Chinese\\')), \\'LANGUAGE\_BIDI\\': False,
      \\'LANGUAGE\_CODE\\': \\'ru\\'}, {}, {\\'perms\\':
      <django.core.context\_processors.PermWrapper object at
      0x1327aa50>, \\'messages\\': [], \\'user\\': <User: root>}, {}]'
    | Original Traceback (most recent call last):
    | File "/usr/lib/python2.5/site-packages/django/template/debug.py",
      line 71, in render\_node
    | result = node.render(context)
    | File
      "/usr/lib/python2.5/site-packages/django/template/\_\_init\_\_.py",
      line 908, in render
    | resolved\_vars = [var.resolve(context) for var in
      self.vars\_to\_resolve]
    | File
      "/usr/lib/python2.5/site-packages/django/template/\_\_init\_\_.py",
      line 687, in resolve
    | value = self.\_resolve\_lookup(context)
    | File
      "/usr/lib/python2.5/site-packages/django/template/\_\_init\_\_.py",
      line 740, in \_resolve\_lookup
    | raise VariableDoesNotExist("Failed lookup for key [%s] in %r",
      (bit, current)) # missing attribute
    | VariableDoesNotExist: Failed lookup for key [request] in
      u'[{\\'action\_index\\': 0, \\'block\\': <Block Node:
      result\_list. Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n \\'>, <django.template.SimpleNode object at
      0x1327a890>, <Text Node: \\'\\n \\'>, <If node>, <Text Node:
      \\'\\n\\'>]>}, {\\'block\\': <Block Node: content. Contents:
      [<Text Node: \\'\\n <div id="content-main"\\'>, <Block Node:
      object-tools. Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n \\'>]>, <Text Node: \\'\\n \\'>, <If node>, <Text
      Node: \\'\\n <div class="module\\'>, <If node>, <Text Node: \\'"
      id="changelist">\\n \\'>, <Block Node: search. Contents:
      [<django.template.InclusionNode object at 0x13243310>]>, <Text
      Node: \\'\\n \\'>, <Block Node: date\_hierarchy. Contents:
      [<django.template.InclusionNode object at 0x132430d0>]>, <Text
      Node: \\'\\n\\n \\'>, <Block Node: filters. Contents: [<Text Node:
      \\'\\n \\'>, <If node>, <Text Node: \\'\\n \\'>]>, <Text Node:
      \\'\\n \\n <form actio\\'>, <If node>, <Text Node: \\'>\\n \\'>,
      <If node>, <Text Node: \\'\\n\\n \\'>, <Block Node: result\_list.
      Contents: [<Text Node: \\'\\n \\'>, <If node>, <Text Node: \\'\\n
      \\'>, <django.template.SimpleNode object at 0x1327a890>, <Text
      Node: \\'\\n \\'>, <If node>, <Text Node: \\'\\n\\'>]>, <Text
      Node: \\'\\n \\'>, <Block Node: pagination. Contents:
      [<django.template.InclusionNode object at 0x13256a90>]>, <Text
      Node: \\'\\n </form>\\n </div>\\'>]>}, {\\'actions\_on\_bottom\\':
      False, \\'cl\\': <django.contrib.admin.views.main.ChangeList
      object at 0x1324ec90>, \\'has\_add\_permission\\': True,
      \\'media\\': <django.forms.widgets.Media object at 0x1327a450>,
      \\'root\_path\\': None, \\'action\_form\\':
      <django.contrib.admin.helpers.ActionForm object at 0x1327a6d0>,
      \\'actions\_on\_top\\': True, \\'app\_label\\': \\'pages\\',
      \\'is\_popup\\': False, \\'title\\':
      u\\'\\\\u0412\\\\u044b\\\\u0431\\\\u0435\\\\u0440\\\\u0438\\\\u0442\\\\u0435
      pages \\\\u0434\\\\u043b\\\\u044f
      \\\\u0438\\\\u0437\\\\u043c\\\\u0435\\\\u043d\\\\u0435\\\\u043d\\\\u0438\\\\u044f\\'},
      {\\'this\_url\\': u\\'/admin/pages/pages/\\'}, {\\'MEDIA\_URL\\':
      \\'/media/\\'}, {\\'LANGUAGES\\': ((\\'ar\\', \\'Arabic\\'),
      (\\'bn\\', \\'Bengali\\'), (\\'bg\\', \\'Bulgarian\\'), (\\'ca\\',
      \\'Catalan\\'), (\\'cs\\', \\'Czech\\'), (\\'cy\\', \\'Welsh\\'),
      (\\'da\\', \\'Danish\\'), (\\'de\\', \\'German\\'), (\\'el\\',
      \\'Greek\\'), (\\'en\\', \\'English\\'), (\\'es\\',
      \\'Spanish\\'), (\\'et\\', \\'Estonian\\'), (\\'es-ar\\',
      \\'Argentinean Spanish\\'), (\\'eu\\', \\'Basque\\'), (\\'fa\\',
      \\'Persian\\'), (\\'fi\\', \\'Finnish\\'), (\\'fr\\',
      \\'French\\'), (\\'ga\\', \\'Irish\\'), (\\'gl\\',
      \\'Galician\\'), (\\'hu\\', \\'Hungarian\\'), (\\'he\\',
      \\'Hebrew\\'), (\\'hi\\', \\'Hindi\\'), (\\'hr\\',
      \\'Croatian\\'), (\\'is\\', \\'Icelandic\\'), (\\'it\\',
      \\'Italian\\'), (\\'ja\\', \\'Japanese\\'), (\\'ka\\',
      \\'Georgian\\'), (\\'ko\\', \\'Korean\\'), (\\'km\\',
      \\'Khmer\\'), (\\'kn\\', \\'Kannada\\'), (\\'lv\\',
      \\'Latvian\\'), (\\'lt\\', \\'Lithuanian\\'), (\\'mk\\',
      \\'Macedonian\\'), (\\'nl\\', \\'Dutch\\'), (\\'no\\',
      \\'Norwegian\\'), (\\'pl\\', \\'Polish\\'), (\\'pt\\',
      \\'Portuguese\\'), (\\'pt-br\\', \\'Brazilian Portuguese\\'),
      (\\'ro\\', \\'Romanian\\'), (\\'ru\\', \\'Russian\\'), (\\'sk\\',
      \\'Slovak\\'), (\\'sl\\', \\'Slovenian\\'), (\\'sr\\',
      \\'Serbian\\'), (\\'sv\\', \\'Swedish\\'), (\\'ta\\',
      \\'Tamil\\'), (\\'te\\', \\'Telugu\\'), (\\'th\\', \\'Thai\\'),
      (\\'tr\\', \\'Turkish\\'), (\\'uk\\', \\'Ukrainian\\'),
      (\\'zh-cn\\', \\'Simplified Chinese\\'), (\\'zh-tw\\',
      \\'Traditional Chinese\\')), \\'LANGUAGE\_BIDI\\': False,
      \\'LANGUAGE\_CODE\\': \\'ru\\'}, {}, {\\'perms\\':
      <django.core.context\_processors.PermWrapper object at
      0x1327aa50>, \\'messages\\': [], \\'user\\': <User: root>}, {}]'

Сделаю небольшое лирическое отступление, \ `treebeard <https://tabo.pe/projects/django-treebeard/docs/1.60/index.html>`__\  это
модуль для django, хранить информацию в виде деревьев. Для решения
задачи создания каталога страниц этот модуль очень подходит. Чем он
хорош: умеет работать по технологиям nested sets, materialized path и
adjacence list. Кроме того, у него отличная документация.

Так вот, из-за плохого чтения оной меня покарал Бог. На домашнем и
находящимся на работе компьютером проблем ни каких не возникало.

В документации четко написано, что если используете в этот модуль в
блоке администраторов, то нужно прописать путь
\ `TEMPLATE\_DIRS <http://docs.djangoproject.com/en/dev/ref/settings/#template-dirs>`__\  до
шаблонов treebeard (в моем случае это
/usr/lib/python2.5/site-packages/treebeard/templates/) а также добавить
\ `django-core-context-processors-request <http://docs.djangoproject.com/en/dev/ref/templates/api/#django-core-context-processors-request>`__\  в
\ `TEMPLATE\_CONTEXT\_PROCESSORS <http://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors>`__\
